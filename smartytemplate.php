<?php

/**
 * The techniques used in this file are by no means the best practices.
 * Most of what is done here (between this file and the image_gallery.tpl)
 * could be much more simple and streamlined but I have intentially employed
 * many aspects of the Smarty<>Drupal integration system to provide examples.
 * Enjoy!
 */

/**
 * overload theme('image_gallery') to call our image_gallery.tpl
 */
function smarty_image_gallery($galleries, $images) {
   return _smarty_callback('image_gallery', array('galleries' => $galleries, 'images' => $images));
}

/**
 * define functions to register for use in your template file.
 *
 * return an associative array of
 * ((function name in template) => (actual function name)) pairs.
 * also excepts a single key as if key and value were the same.
 */
function smarty_register_functions(){
   return array(
      'gen_gallery_link' => 'smarty_gen_gallery_link',
      'gen_image_link' => 'smarty_gen_image_link',
      'format_plural' => 'wrap_format_plural',
      'add_image_gallery_pager',
      'assign_is_empty',
   );
}

/**
 * called from image_gallery.tpl
 *
 * separated to beautify image_gallery.tpl
 */
function smarty_gen_gallery_link($params, &$smarty) {
    $gallery = $params['gallery'];
    $result = l(image_display($gallery->latest, 'thumbnail'), 'image/tid/'.$gallery->tid, array(), NULL, NULL, FALSE, TRUE);
    $result .= check_markup($gallery->description);
    return $result;
}

function smarty_gen_image_link($params, &$smarty) {
    $image = &$params['image'];
    $result = l(image_display($image, 'thumbnail'), 'node/'.$image->nid, array(), NULL, NULL, FALSE, TRUE);
    return $result;
}

function add_image_gallery_pager() {
    if ($pager = theme('pager', NULL, variable_get('image_images_per_page', 6), 0)) {
      return $pager;
    }
}

function assign_is_empty($params, &$smarty) {
    $images = $params['images'];
    $galleries = $galleries['images'];
    $is_empty = count($images) + count($galleries) == 0;
    $smarty->assign('is_empty', $is_empty);
}

/**
 * example for generic existing function wrap
 */
function wrap_format_plural($params, &$smarty) {
    return call_user_func_array('format_plural', $params);
}
?>