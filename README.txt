
box_grey_smarty theme for Smarty on Drupal
v 4.6
----------------------------------------

box_grey_smarty is the port of the box_grey phptemplate theme to Smarty.
Please refer to the box_grey documentation.