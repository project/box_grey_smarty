{**
 * image_gallery.tpl
 * see smartytemplate.php for definition and regisration of custom functions.
 *}
{if count($galleries)}
<ul class="galleries">
{foreach from=$galleries item=gallery}
  {if $gallery->count > -1}
  <li>
    <h3>{l text=$gallery->name href="image/tid/`$gallery->tid`"}</h3>
    <div class="description">{gen_gallery_link gallery=$gallery}</div>
      <p class="count">{format_plural num=$gallery->count string1='There is %count image in this gallery' string2='There are %count images in this gallery'}</p>
    {if $gallery->latest->changed}
      <p class="last">
    {* to access template variables within {php} blocks use $this->vars['variable_name'] *}
    {php} return t('Last updated: %date', array('%date' => format_date($this->vars['gallery']->latest->changed))); {/php}
      </p>
    {/if}
  </li>
  {/if}    
{/foreach}
</ul>
{/if}

{if count($images)}
<ul class="images">
{foreach from=$images item=image}
  <li{if $image->sticky} class="sticky"{/if}>{gen_image_link image=$image}</li>
{/foreach}  
</ul>
{/if}

{add_image_gallery_pager}

{assign_is_empty images=$images galleries=$galleries}
{if $is_empty}
  <p class="count">{format_plural num=0 string1='There is %count image in this gallery' string2='There are %count images in this gallery'}</p>
{/if}
  