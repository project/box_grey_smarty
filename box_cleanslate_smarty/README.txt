
box_cleanslate_smarty variant for box_grey_smarty theme
-----------------------------------------

This is a port of the box_cleanslate theme (no changes were made except for naming in this file)

This style is essentially the phptemplate cleanslate theme moved onto the box_grey_smarty template.
It's not identical to cleanslate but uses the same colours and similar borders.


Authors
------
tclineks - ported form box_cleanslate to box_cleanslate_smarty (no serious changes were made)
mailto: travis dot cline at gmail dot com

adrinux - original author
mailto: adrinux@ gmail.com
IM: perlucida

Known Problems
--------------
See the box_grey README
