<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="{$language}" xml:lang="{$language}">
<head>
  <title>{$head_title}</title>
  <meta http-equiv="Content-Style-Type" content="text/css" />
  {$head}
  {$styles}
</head>
<body{$onload_attributes}>
<div id="header">
  {$search_box}
  {if $logo}
  <a href="{url}" title="Index Page"><img src="{$logo}" alt="Logo" /></a>
  {/if}
  {if $site_name}
    <h1 id="site-name"><a href="{url}" title="Index Page">{$site_name}</a></h1>
  {/if}
  {if $site_slogan}
    <span id="site-slogan">{$site_slogan}</span>
  {/if}
  <br class="clear" />
  {if $header}
    <div id="header_region">{$header}</div>
  {/if}
</div>
<div id="top-nav">
  {if count($secondary_links)}
    <ul id="secondary">
      <li>{theme function='links' data=$secondary_links delimiter="</li>\n      <li>"}</li>
    </ul>
  {/if}

  {if count($primary_links)}
    <ul id="primary">
      <li>{theme function='links' data=$primary_links delimiter="</li>\n      <li>"}</li>
    </ul>
  {/if}
</div>
<table id="content">
  <tr>
    {if $sidebar_left ne ""}
      <td class="sidebar" id="sidebar-left">
        {$sidebar_left}
      </td>
    {/if}
        <td class="main-content" id="content-{$layout}">
        {if $title ne ""}
          <h2 class="content-title">{$title}</h2>
        {/if}
        {if $tabs ne ""}
          {$tabs}
        {/if}

        {if $mission ne ""}
          <div id="mission">{$mission}</div>
        {/if}

        {if $help ne ""}
          <p id="help">{$help}</p>
        {/if}

        {if $messages ne ""}
          <div id="message">{$messages}</div>
        {/if}

        <!-- start main content -->
        {$content}
        <!-- end main content -->
        </td><!-- mainContent -->
    {if $sidebar_right ne ""}
    <td class="sidebar" id="sidebar-right">
        {$sidebar_right}
    </td>
    {/if}
  </tr>
</table>
{$breadcrumb}
<div id="footer">
  {if $footer_message}
    <p>{$footer_message}</p>
  {/if}
Validate <a href="http://validator.w3.org/check/referer">XHTML</a> or <a href="http://jigsaw.w3.org/css-validator/check/referer">CSS</a>.
</div><!-- footer -->
 {$closure}
  </body>
</html>

